import hmac
import base64
import hashlib
import logging
from collections import OrderedDict
from . import PaymentGateway

logger = logging.getLogger(__name__)


class EcommPayGateway(PaymentGateway):

    def __init__(self, healthcheck_url, project_id, secret_key, force_payment_method):
        self._project_id = project_id
        self._secret_key = secret_key
        self._healthcheck_url = healthcheck_url
        self._force_payment_method = force_payment_method

    def get_deposit_request_data(self, order_id, amount,
                                 currency, comment, profile_id,
                                 return_url, cancel_url):
        merchant_params = {
            'force_payment_method': self._force_payment_method,
            'payment_id': order_id,
            'payment_amount': basic_to_minor_units_amount(amount),
            'payment_currency': currency,
            'project_id': self._project_id,
            'payment_description': comment,
            'customer_id': profile_id,
            'merchant_fail_url': cancel_url,
            'merchant_success_url': return_url.format(**{'order_id': order_id}),
        }

        payload = self.get_params_to_sign(merchant_params)
        signature = self.generate_signature(payload)

        merchant_params['signature'] = signature

        return merchant_params

    def get_withdrawal_request_data(self, order_id, amount, currency, ip_address, token):
        merchant_params = {
            'project_id': self._project_id,
            'payment_currency': currency,
            'payment_id': order_id,
            'ip_address': ip_address,
            'payment_amount': basic_to_minor_units_amount(amount),
            'token': token
        }

        payload = self.get_params_to_sign(merchant_params)
        signature = self.generate_signature(payload)

        merchant_params['signature'] = signature

        return merchant_params

    @property
    def get_payment_url(self):
        domain = self._make_automated_rest_request(url=self._healthcheck_url, data={}).text.strip()
        url = f'https://{domain}/payment'
        return url

    def get_params_to_sign(self, params: dict, prefix='', sort=True) -> dict:
        params_to_sign = {}

        for key in params:
            param_key = prefix + (':' if prefix else '') + key
            value = params[key]

            if isinstance(value, list):
                value = {str(key): value for key, value in enumerate(value)}

            if isinstance(value, dict):
                sub_array = self.get_params_to_sign(value, param_key, False)
                params_to_sign.update(sub_array)
            else:
                if isinstance(value, bool):
                    value = '1' if value else '0'
                else:
                    value = str(value)
                params_to_sign[param_key] = param_key + ':' + value

        if sort:
            sorted(params_to_sign.items(), key=lambda item: item[0])

        return params_to_sign

    def generate_signature(self, payload):
        params_to_sign_list = list(OrderedDict(sorted(payload.items(), key=lambda t: t[0])).values())
        string_to_sign = ';'.join(params_to_sign_list).encode()
        return base64.b64encode(hmac.new(self._secret_key.encode(), string_to_sign, hashlib.sha512).digest()).decode()

    def make_withdrawal(self, order_id, amount, token, ip_address, currency):
        merchant_params = self.get_withdrawal_request_data(order_id=order_id,
                                                           amount=amount,
                                                           ip_address=ip_address,
                                                           currency=currency,
                                                           token=token,
                                                           )
        url = self.get_payment_url + 'card/payout/token'  # todo вынести в константы
        resp = self._make_automated_rest_request(url=url, data=merchant_params, method='POST')
        return resp.json()
