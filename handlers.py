import logging

logger = logging.getLogger(__name__)


class EcommPayDepositTransactionHandler(GatewayWalletTransactionHandler):

    def __init__(self, transaction, wallet, gateway, data: dict, context=None):
        super().__init__(transaction, wallet, gateway, context=context)
        self._data = data

    def handle(self):
        """
        Handle transaction.
        """

        order_id = self.transaction.order_id
        gateway: EcommPayGateway = self.gateway

        try:
            payment = self._data['payment']
            payment_signature = self._data['signature']
            status = payment['status']
            payment_amount = minor_units_to_basic_amount(int(payment['sum']['amount']))
            payment_currency = payment['sum']['currency']
        except KeyError as e:
            logging.exception(e)
            raise TransactionProcessError('Invalid transaction data.')

        if not all([payment_amount == self.transaction.amount,
                    payment_currency == self.transaction.currency_id,
                    ]):
            msg = f'Transaction: {self.transaction.amount} {self.transaction.currency_id} ' \
                  f'Response: {payment_amount} {payment_currency}'

            logging.exception(msg)
            raise EcommPayValidationError(msg)

        payload = self._data
        payload.pop('signature')

        params_to_sign = gateway.get_params_to_sign(payload)
        signature = gateway.generate_signature(payload=params_to_sign)
        if signature != payment_signature:
            raise TransactionProcessError(
                'Invalid status response.'
            )

        comment = EcommPayDepositComment(context={'order_id': order_id})

        if status != constants.STATUS_PROCESSED:
            raise TransactionProcessError(f'Invalid status value. Expected '
                                          f'"{constants.STATUS_PROCESSED}", got "{status}".')

        self.transaction.comment = str(comment)
        self.transaction.response = self._data
        self.transaction.save()

        super().handle()


class EcommPayWithdrawalTransactionHandler(GatewayTransactionHandler):

    def __init__(self, transaction, gateway, token, client_ip_address, context=None):
        super().__init__(transaction, gateway, context=context)
        self._token = token
        self._client_ip_address = client_ip_address

    def handle(self):
        """
        Handle transaction.
        """

        order_id = self.transaction.order_id
        ip_address = self.context.get('ip_address')
        comment = EcommPayWithdrawalComment(context={'order_id': order_id})

        gateway: EcommPayGateway = self.gateway

        amount = self.transaction.amount

        self.transaction.comment = str(comment)
        self.transaction.ip = ip_address

        try:

            response_data = gateway.make_withdrawal(order_id=order_id, amount=amount,
                                                    token=self._token,
                                                    ip_address=self._client_ip_address,
                                                    currency=self.transaction.currency_id)

            self.transaction.response = response_data
            self.transaction.save()

        except TransactionWithdrawalGatewayError as e:
            raise TransactionWithdrawalGatewayError(str(e))  # re-raise exception

        finally:
            self.transaction.save()
